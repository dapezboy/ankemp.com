<!DOCTYPE html>
<html class="uk-height-1-1">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <title>About - Andrew Kemp</title>
        <meta charset="UTF-8">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="js/base.js"></script>
        <script src="js/ankemp.js"></script>
        <link rel="stylesheet" href="css/base.css" />
        <link rel="stylesheet" href="css/site.css" />
    </head>
    <body class="uk-height-1-1">
        <div class="uk-vertical-align uk-text-center" style="height: 100%; width: 100%;">
            <div class="uk-grid uk-vertical-align-middle" style="max-width: 572px;">
                <div class="uk-width-1-1">
                    <div class="uk-panel-box uk-panel-box-tertiary uk-margin-bottom-remove">
                        <h1 class="uk-text-primary">About Me</h1>
                        <div class="uk-text-left">I'm Andrew, I'm a web developer &amp; WordPress enthusiast. I've been developing as a hobby for about 10 years, and 6 years professionally, I mainly focus on php, javascript and its many frameworks, and MySQL. I'm highly addicted to WordPress and using it everywhere I can, and am slowing dipping my toes into Python.</div>
                    </div>
                </div>
                <div class="uk-width-1-1">
                    <nav class="uk-navbar uk-navbar-attached">
                        <div class="uk-navbar-content uk-visible-small uk-navbar-center">
                            <a id="p-navl" class="uk-link-muted"><i class="uk-icon-bars uk-icon-medium"></i></a>
                        </div>
                        <ul class="uk-navbar-nav uk-hidden-small">
                            <li><a href="./"><i class="uk-icon-home uk-icon-small uk-text-primary"></i></a></li>
                            <li><a href="./portfolio">Portfolio</a></li>
                            <li class="uk-active"><a href="./about">About</a></li>
                            <li><a href="./contact">Contact</a></li>
                        </ul>
                    </nav>
                    <div id="p-nav" class="uk-panel-box uk-panel-box-tertiary-flip uk-animation-fade uk-hidden">
                        <a class="uk-button uk-button-primary" href="./"><i class="uk-icon-home uk-icon-small"></i></a>
                        <a class="uk-button uk-button-primary" href="./portfolio">Portfolio</a>
                        <a class="uk-button uk-button-primary uk-active" href="./about">About</a>
                        <a class="uk-button uk-button-primary" href="./contact">Contact</a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
