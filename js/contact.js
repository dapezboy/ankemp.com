$(function() {
    $("#emailme").validate({
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            message: {
                required: true
            }
        },
        messages: {
            name: "I gotta know your name",
            email: {
                required: "I can't send it without one",
                email: "That email isn't gonna work"
            },
            message: "Well, what would be the point of this with it blank?"
        },
        highlight: function(element, errorClass) {
            $(element).addClass("uk-form-danger");
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass("uk-form-danger");
        },
        submitHandler: function() {
            var dataset = $("#emailme").serialize();
            $.ajax({
                type: "POST",
                url: "esgo.php",
                data: dataset,
                success: function(data) {
                    console.log(data);
                    $.UIkit.notify("<i class='uk-icon-check'> Your email has been sent!</i>", {status:'success'});
                    $("#emailme").trigger('reset')
                }
            });
        }
    });
});