function Main() {
	this.portLoad = function(project,id) {
		jQuery.getJSON(project, function(json) {
			if(json.badge){
				$(id + "-badge").html(json.badge);
			} else {
				$(id + "-badge").remove();
			}
			if(json.teaser) {
				$(id + "-teaser img").attr("src","images/teasers/"+json.teaser);
			} else {
				$(id + "-teaser").remove();
			}
			$(id + "-title").html(json.title);
			$(id + "-teaser img").attr("src","images/teasers/"+json.teaser);
			$(id + "-specs").html("<strong>Company:</strong> " + json.company);
			$(id + "-specs").append("<br /><strong>Technology:</strong> " + json.technology);
			$(id + "-content").html(json.content);
			if(json.source){
				$(id + "-source").attr("href","#code-sample");
			} else {
				$(id + "-source").removeAttr("id").addClass("uk-button-disabled");
			}
			if(json.view){
				$(id + "-view").attr("href",json.view);
			} else {
				$(id + "-view").addClass("uk-button-disabled");
			}
			$(id + "-source").click(function() {
				var modal = new $.UIkit.modal("#code-sample");
				modal.show();
				$("#sample").attr("src", "http://pastebin.com/embed_iframe.php?i=" + json.source);
			});
		});
	};
	this.pNav = function() {
		$("nav.uk-navbar").toggleClass("uk-navbar-attached-full");
		$("#p-nav").toggleClass("uk-hidden");
	};
}
var ank = new Main();

$(function() {
	$("#p-navl").click(function() {ank.pNav();});
});