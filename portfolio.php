<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <title>Portfolio - Andrew Kemp</title>
        <meta charset="UTF-8">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="js/base.js"></script>
        <script src="js/ankemp.js"></script>
        <link rel="stylesheet" href="css/base.css" />
        <link rel="stylesheet" href="css/site.css" />
    </head>
    <body>
        <header>
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <div class="uk-panel-box uk-panel-box-tertiary uk-margin-bottom-remove">
                        <div class="uk-grid">
                            <div class="uk-width-1-2">
                                <h1 class="uk-margin-bottom-remove">Andrew Kemp</h1>
                            </div>
                            <div class="uk-width-1-2 uk-float-right">
                                <div class="uk-text-right">(808) 492-4120</div>
                                <div class="uk-text-right">andrew@ankemp.com</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-1">
                    <nav class="uk-navbar uk-navbar-attached">
                        <div class="uk-navbar-content uk-visible-small uk-navbar-center">
                            <a id="p-navl" class="uk-link-muted"><i class="uk-icon-bars uk-icon-medium"></i></a>
                        </div>
                        <ul class="uk-navbar-nav uk-hidden-small">
                            <li><a href="./"><i class="uk-icon-home uk-icon-small uk-text-primary"></i></a></li>
                            <li class="uk-active"><a href="./portfolio">Portfolio</a></li>
                            <li><a href="./about">About</a></li>
                            <li><a href="./contact">Contact</a></li>
                        </ul>
                    </nav>
                    <div id="p-nav" class="uk-panel-box uk-panel-box-tertiary-flip uk-text-center uk-animation-fade uk-hidden">
                        <a class="uk-button uk-button-primary" href="./"><i class="uk-icon-home uk-icon-small"></i></a>
                        <a class="uk-button uk-button-primary uk-active" href="./portfolio">Portfolio</a>
                        <a class="uk-button uk-button-primary" href="./about">About</a>
                        <a class="uk-button uk-button-primary" href="./contact">Contact</a>
                    </div>
                </div>
            </div>
        </header>
        <div class="uk-container uk-container-center">
            <ul class="uk-grid uk-grid-preserve uk-margin-small-top uk-margin-bottom portfolio" data-uk-grid-margin data-uk-grid-match>
                <?php $i = 0; foreach(glob('content/*.json') as $project) : $i++; ?>
                <li class="uk-width-1-1">
                    <div id="<?php echo $i; ?>" class="uk-panel">
                        <div id="<?php echo $i; ?>-teaser" class="uk-panel-teaser">
                            <img class="uk-width-1-1" src="" />
                        </div>
                        <div id="<?php echo $i; ?>-box" class="uk-panel-box uk-panel-box-tertiary-flip content">
                            <div id="<?php echo $i; ?>-badge" class="uk-panel-badge uk-badge"></div>
                            <h3 id="<?php echo $i; ?>-title" class="uk-panel-title uk-margin-bottom-remove"></h3>
                            <div id="<?php echo $i; ?>-specs" class="uk-text-small"></div>
                            <div id="<?php echo $i; ?>-content"></div>
                            <div class="uk-grid uk-margin-top uk-text-center">
                                <div class="uk-width-1-2"><a id="<?php echo $i; ?>-source" class="uk-button uk-button-primary">Source</button></a></div>
                                <div class="uk-width-1-2"><a id="<?php echo $i; ?>-view" target="_blank" class="uk-button uk-button-primary">View</button></a></div>
                            </div>
                        </div>
                    </div>
                    <script>ank.portLoad("<?php echo $project; ?>", "#<?php echo $i; ?>");</script>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div id="code-sample" class="uk-modal">
            <div class="uk-modal-dialog">
                <a class="uk-modal-close uk-close"></a>
                <iframe id="sample" class="uk-scrollable-text" style="border:none;height:650px;width:100%">Loading...</iframe>
            </div>
        </div>
    </body>
</html>
