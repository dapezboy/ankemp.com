<!DOCTYPE html>
<html class="uk-height-1-1">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <title>Contact - Andrew Kemp</title>
        <meta charset="UTF-8">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="js/base.js"></script>
        <script src="js/addons/notify.min.js"></script>
        <script src="js/validate.min.js"></script>
        <script src="js/ankemp.js"></script>
        <script src="js/contact.js"></script>
        <link rel="stylesheet" href="css/base.css" />
        <link rel="stylesheet" href="css/addons.css" />
        <link rel="stylesheet" href="css/site.css" />
    </head>
    <body class="uk-height-1-1">
        <div class="uk-vertical-align uk-text-center" style="height: 100%; width: 100%;">
            <div class="uk-grid uk-vertical-align-middle" style="max-width: 572px;">
                <div class="uk-width-1-1">
                    <div class="uk-panel-box uk-panel-box-tertiary uk-margin-bottom-remove">
                        <h1 class="uk-text-primary">Contact Me</h1>
                        <form id="emailme" class="uk-form uk-margin-bottom">
                            <div class="uk-grid" data-uk-margin>
                                <div class="uk-width-1-1"><input type="text" name="name" placeholder="Name" class="uk-width-1-1" required></div>
                                <div class="uk-width-1-1"><input type="text" name="email" placeholder="Email" class="uk-width-1-1" required></div>
                                <div class="uk-width-1-1"><textarea name="message" placeholder="Message" class="uk-width-1-1" required></textarea></div>
                                <input type="hidden" name="chk" value="" />
                                <div class="uk-width-1-1"><input type="submit" name="sendemail" value="Send" class="uk-button uk-button-primary uk-width-1-1"></div>
                            </div>
                        </form>
                        <hr />
                        <a href="//www.facebook.com/andrewnkemp" target="_blank" class="uk-icon-button uk-icon-facebook"></a>
                        <a href="//google.com/+AndrewKempN" target="_blank" class="uk-icon-button uk-icon-google-plus"></a>
                        <a href="//www.linkedin.com/in/ankemp" target="_blank" class="uk-icon-button uk-icon-linkedin"></a>
                        <a href="//github.com/andrewkdesigns" target="_blank" class="uk-icon-button uk-icon-github"></a>
                    </div>
                </div>
                <div class="uk-width-1-1">
                    <nav class="uk-navbar uk-navbar-attached">
                        <div class="uk-navbar-content uk-visible-small uk-navbar-center">
                            <a id="p-navl" class="uk-link-muted"><i class="uk-icon-bars uk-icon-medium"></i></a>
                        </div>
                        <ul class="uk-navbar-nav uk-hidden-small">
                            <li><a href="./"><i class="uk-icon-home uk-icon-small uk-text-primary"></i></a></li>
                            <li><a href="./portfolio">Portfolio</a></li>
                            <li><a href="./about">About</a></li>
                            <li class="uk-active"><a href="./contact">Contact</a></li>
                        </ul>
                    </nav>
                    <div id="p-nav" class="uk-panel-box uk-panel-box-tertiary-flip uk-animation-fade uk-hidden">
                        <a class="uk-button uk-button-primary" href="./"><i class="uk-icon-home uk-icon-small"></i></a>
                        <a class="uk-button uk-button-primary" href="./portfolio">Portfolio</a>
                        <a class="uk-button uk-button-primary" href="./about">About</a>
                        <a class="uk-button uk-button-primary uk-active" href="./contact">Contact</a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
